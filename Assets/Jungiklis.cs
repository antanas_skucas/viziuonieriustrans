﻿using UnityEngine;
using System.Collections;

public class Jungiklis : MonoBehaviour {
	 
	//public GameObject electroMark;
	public GameObject electroMarkUi;
	public Renderer electroMark;

	public GameObject ermiUi;
	public Renderer ermi;

	// Update is called once per frame
	void Update () {

		electroMarkUi.SetActive(electroMark.isVisible);
		if(!electroMark.isVisible){
			ermiUi.SetActive (ermi.isVisible);
		} else {
			ermiUi.SetActive (false);
		}

	}
}
