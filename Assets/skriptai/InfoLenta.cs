﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InfoLenta : MonoBehaviour {

	public int stotele;
	public GameObject meniu;
	public GameObject error;
	public Color[] spalvos;
	public Text laikrodis;
	public Text stotelesPavadinimas;
	public transportas[] transInfo;
	public transportas mainTekst;

	[System.Serializable]
	public class transportas{
		public Text tekstas;
		public Text marsrutoNumeris;
		public Image spava;

	}

	private int kiekLenteliu;
	private int last;
	private int lastStotele;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		stotele = StotelesInformacija.stotelesNr;
		KiekLenteliu ();
		if (StotelesInformacija.stotelesNr == 0 || kiekLenteliu == 0) { 
			OffOn (false);
		} else {
			OffOn (true);
			UpdateData ();
			OffNenaudojamas ();
		}
	}



	void UpdateData() {
		laikrodis.text = LaikasDabar.laikas;
		Vector3[] trans = StotelesInformacija.trans;
		string[] transName = StotelesInformacija.transName;

		if(stotelesPavadinimas)stotelesPavadinimas.text = StotelesInformacija.pavadinimas;

		for (int i = 0; i < kiekLenteliu; i++) {
			transInfo [i].tekstas.text = "už "+trans [i].x + " min."; 
			transInfo [i].marsrutoNumeris.text = transName[i];
			transInfo [i].spava.color = spalvos[(int)trans [i].y-1];
		}

		mainTekst.tekstas.text = "už "+trans [0].x + " min."; 
		mainTekst.marsrutoNumeris.text = transName[0];
		mainTekst.spava.color = spalvos[(int)trans [0].y-1];
	}
	void OffOn(bool busena) {
		print ("bandom isjungti: "+busena);
		meniu.SetActive (busena);
		error.SetActive (!busena);
	}

	void KiekLenteliu() {
		kiekLenteliu = transInfo.Length;
		if (kiekLenteliu > StotelesInformacija.transName.Length-1) {
			kiekLenteliu = StotelesInformacija.transName.Length-1;
		}
	}

	void OffNenaudojamas() {
		for (int i = 0; i < transInfo.Length; i++) {
			if (i+1 > kiekLenteliu){
				transInfo [i].tekstas.gameObject.SetActive (false); 
			} else {
				transInfo [i].tekstas.gameObject.SetActive (true); 
			}
		}
	}

}
