﻿/*
	CSVReader by Dock. (24/8/11)
	http://starfruitgames.com
 
	usage: 
	CSVReader.SplitCsvGrid(textString)
 
	returns a 2D string array. 
 
	Drag onto a gameobject for a demo of CSV parsing.
*/

using UnityEngine;
using System.Collections;
using System.Linq; 

public class CSVReader : MonoBehaviour 
{
	public TextAsset csvFile; 
	public void Start()
	{
		string[,] grid = SplitCsvGrid(csvFile.text);
		Debug.Log("size = " + (1+ grid.GetUpperBound(0)) + "," + (1 + grid.GetUpperBound(1))); 

		DebugOutputGrid(grid); 
	}

	// outputs the content of a 2D array, useful for checking the importer
	static public void DebugOutputGrid(string[,] grid)
	{
		string textOutput = ""; 
		for (int y = 0; y < grid.GetUpperBound(1); y++) {	
			for (int x = 0; x < grid.GetUpperBound(0); x++) {

				textOutput += grid[x,y]; 
				textOutput += "|"; 
			}
			textOutput += "\n"; 
		}
		Debug.Log(textOutput);
	}

	// 


	// splits a CSV file into a 2D string array
	static public string[,] SplitCsvGrid(string csvText)
	{
		string[] lines = csvText.Split("\n"[0]); 
		string[] eiluciuPavadinimai = SplitCsvLine(lines[0]);

		// apskaičiuojame kiek stotelių tiksliau
		int plotis = eiluciuPavadinimai.Length;
		int ilgis = lines.Length;
		while (lines [ilgis-1].Length <2  && ilgis > 0) {
			ilgis--;
		}
		while (eiluciuPavadinimai[plotis-1].Length == 0 && plotis > 0) {
			plotis--;
		}

		Debug.Log ("New data " + plotis + " / " +ilgis);
		// creates new 2D string grid to output to
		string[,] outputGrid = new string[plotis, ilgis]; 
		for (int y = 0; y < ilgis; y++)
		{
			string[] row = SplitCsvLine( lines[y] ); 
			for (int x = 0; x < plotis; x++) 
			{
				outputGrid[x,y] = row[x]; 

				// This line was to replace "" with " in my output. 
				// Include or edit it as you wish.
				outputGrid[x,y] = outputGrid[x,y].Replace("\"\"", "\"");
			}
		}
		return outputGrid; 
	}

	//skirtas nustatyti kiek reiksmiu, nes kitaip neveiia
	static public void saveList(string[]data, string vieta, string pavadinimas) {
		string textData = "";
		for (int y = 0; y < data.Length; y++) {
			textData += data[y]+"\n";
		}

		System.IO.File.WriteAllText (vieta + pavadinimas + ".csv", textData);
		print (vieta + pavadinimas);
	}

	static public string[,] SplitStops(string csvText, int plotis = 6)
	{
		string[] lines = csvText.Split("\n"[0]); 
		string[] eiluciuPavadinimai = SplitCsvLine(lines[0]);

		// apskaičiuojame kiek stotelių tiksliau

		int ilgis = lines.Length;
		while (lines [ilgis-1].Length <2  && ilgis > 0) {
			ilgis--;
		}

		Debug.Log ("New data " + plotis + " / " +ilgis);
		// creates new 2D string grid to output to
		string[,] outputGrid = new string[plotis, ilgis]; 
		for (int y = 0; y < ilgis; y++)
		{
			string[] row = SplitCsvLine( lines[y] ); 
			for (int x = 0; x < plotis; x++) 
			{
				outputGrid[x,y] = row[x]; 

				// This line was to replace "" with " in my output. 
				// Include or edit it as you wish.
				outputGrid[x,y] = outputGrid[x,y].Replace("\"\"", "\"");
			}
		}
		return outputGrid; 
	}

	static public string[,] SplitCsvGridOld(string csvText)
	{
		string[] lines = csvText.Split("\n"[0]); 

		// finds the max width of row
		int width = 0; 
		for (int i = 0; i < lines.Length; i++)
		{
			string[] row = SplitCsvLine( lines[i] ); 
			width = Mathf.Max(width, row.Length); 
		}

		// creates new 2D string grid to output to
		string[,] outputGrid = new string[width + 1, lines.Length + 1]; 
		for (int y = 0; y < lines.Length; y++)
		{
			string[] row = SplitCsvLine( lines[y] ); 
			for (int x = 0; x < row.Length; x++) 
			{
				outputGrid[x,y] = row[x]; 

				// This line was to replace "" with " in my output. 
				// Include or edit it as you wish.
				outputGrid[x,y] = outputGrid[x,y].Replace("\"\"", "\"");
			}
		}

		return outputGrid; 
	}
		

	// splits a CSV row 
	static public string[] SplitCsvLine(string line)
	{
		return (from System.Text.RegularExpressions.Match m in System.Text.RegularExpressions.Regex.Matches(line,
			@"(((?<x>(?=[,\r\n]+))|""(?<x>([^""]|"""")+)""|(?<x>[^,\r\n]+)),?)", 
			System.Text.RegularExpressions.RegexOptions.ExplicitCapture)
			select m.Groups[1].Value).ToArray();
	}
}