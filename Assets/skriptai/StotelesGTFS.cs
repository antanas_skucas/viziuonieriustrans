﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class StotelesGTFS : MonoBehaviour {

	public bool saveAll;
	//GTFS data
	public TextAsset stops;
	public TextAsset stopTimes;
	public TextAsset trips;
	public TextAsset calendar;

	//kitka
	public Text infoText;
	public string artimiausiaStotele;

	//static
	static public string[,] stoteles; //1 - eilute pagal pavadinima 2-turinys;
	static public string[,] sustojimuLaikai;//informacija apie sustojimus
	static public string[,] keliones;//marsrutu informacija
	static public string[,] kalendorius;//marsrutu informacija
	static public int ilgis;//kiek stoteliu
	static public int plotis;//kiek yra informacijos
	static public Vector2[] stotelesXY; //stoteles padetis metrais
	public int stotele; // artimiausios stoteles id
	public int stoteleOld;// priestai nustatytos stoteles id
	//static public Vector2[] stotelesXYTest;
	static public Vector2[] stotelesLonLan;//stoteles padetis lon
	static public int[] id;
	static public string[] stotelesPavadinimas;

	void Update() {
		if (LocationService.position == LocationService.lastPosition)return;
		//myLocation = LocationService.position;
		stotele = ArtimiausiaStotelė (LocationService.position);
		artimiausiaStotele = stotelesPavadinimas [stotele];


		Stoteleslaikai(id [stotele]);


		if (infoText)infoText.text = artimiausiaStotele;

	}

	int ArtimiausiaStotelė (Vector2 vieta) {
		float atstumas = 100f;
		int artimStotele = 0;

		for (int i = 1; i < ilgis; i++) {
			//stotelesXYTest[i] = stotelesXY[i]-vieta;
			float a = Vector2.Distance(vieta, stotelesXY[i]);
			if (a < atstumas) {
				atstumas = a;
				artimStotele = i;
			}
		}
		//if(atstumas>40f) artimStotele = 0;
		return artimStotele;
	}

	void Start () {
		sustojimuLaikai = CSVReader.SplitStops(stopTimes.text, 4);// didziulis failas // laikas su sustojimu
		stoteles = CSVReader.SplitStops(stops.text, 6);
		keliones =  CSVReader.SplitStops(trips.text, 4);
		kalendorius = CSVReader.SplitStops(calendar.text, 10);
		plotis = stoteles.GetLength (0);
		ilgis = stoteles.GetLength (1);

		Debug.Log (ilgis);

		stotelesPavadinimas = new string[ilgis];
		stotelesLonLan = new Vector2[ilgis];
		stotelesXY = new Vector2[ilgis];
		//stotelesXYTest  = new Vector2[ilgis];
		id = new int[ilgis];

		//stotelių pavadinimai lokacijos id
		for (int i = 1; i < ilgis; i++) {
			stotelesLonLan[i] = new Vector2(float.Parse(stoteles[4,i]), float.Parse(stoteles[5,i]));
			stotelesXY [i] = GpsPlacer.LatLonToMetersNew (stotelesLonLan [i].x, stotelesLonLan [i].y);
			stotelesPavadinimas [i] = stoteles [2, i];
			id[i] = int.Parse (stoteles [0, i]);//stoteles id naudojamas atpažinimui
		}
		SaveAll ();
	}

	void SaveAll() {
		for (int i = 1; i < id.Length; i++) {
			Stoteleslaikai (id[i]);
		}
		saveAll = false;

	}

	//stoteles marsrutai su laikais
	public string[] stotelesLaikai;

	void Stoteleslaikai(int stotele) {
		if (!saveAll) {
			if (stotele == 0 || stotele == stoteleOld)
				return;
			stoteleOld = stotele;
		}

		List<int> sarasas = new List<int> ();
		Debug.Log ("svarbu: " + sustojimuLaikai [3, 2]);
		for (int i = 1; i < sustojimuLaikai.GetLength (1); i++) {
			if (stotele == int.Parse (sustojimuLaikai [3, i])) {
				sarasas.Add (i);
			}
		}

		// kai žinome kurie marsrutai kasamo duomenis toliau
		stotelesLaikai = new string[sarasas.Count+1];
		stotelesLaikai [0] = "timeInMin,trip_id,service_id,tipas,numeris,pirmadienis,antradienis,treciadienis,ketvirtadienis,penktadienis,sestadienis,sekmadienis ";
		for (int y = 1; y < stotelesLaikai.Length; y++) {
			string tripId = sustojimuLaikai [0, sarasas [y - 1]];

			//pridedam laiką minutėmis;
			string[] laikas = sustojimuLaikai [1, sarasas [y-1]].Split(":"[0]);
			stotelesLaikai [y] += float.Parse(laikas[0])*60+float.Parse(laikas[1]) +",";

			//pridedam marsruto id
			stotelesLaikai [y] += tripId +",";

			//pridedam service_id
			int kelionesNr = SurastiMarstuta(tripId);
			string serviceId = keliones [1, kelionesNr];
			stotelesLaikai [y] += serviceId + ",";

			//tipas, numeris
			string[] data = keliones [0, kelionesNr].Split("_"[0]);
			stotelesLaikai [y] += data[1] + "," + data[2] + ",";

			//kuriomis dienomis vaziuoja
			stotelesLaikai [y] += kuriomisDarboDienomis(serviceId);
		}
		string path = Application.dataPath+"/data/";

		CSVReader.saveList(stotelesLaikai, path, stotele+"");


	}

	string kuriomisDarboDienomis(string serviceId){
		for (int y = 1; y < kalendorius.GetLength (1); y++) {
			if (serviceId == kalendorius [0, y]) {
				return kalendorius [1, y]+","+kalendorius [2, y]+","+kalendorius [3, y]+","+kalendorius [4, y]+","+kalendorius [5, y]+","+kalendorius [6, y]+","+kalendorius [7, y];
			}
		}
		//return "neveikia";
		return "0,0,0,0,0,0,0";
	}


	int SurastiMarstuta(string tripId) {
		for (int y = 1; y < keliones.GetLength (1); y++) {
			if (tripId == keliones [2, y]) {
				return y;
			}
		}
		return -1;
	}





}

