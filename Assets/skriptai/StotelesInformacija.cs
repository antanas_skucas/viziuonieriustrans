﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class StotelesInformacija : MonoBehaviour {

	static public int stotelesNr;
	static public string pavadinimas;

	//public int stoteleInf;
	public int stotelesNrOld;
	private  TextAsset stoteleData;

	//private
	private int[] transportasLaikas;
	private  string[] transportoNumeris;//pvz 46 autobusas
	private  int[] transportoTipas; //1-autobusas, 2-troleibusas, 3-greitasis, 4-naktinis
	private int[] transportasServiceId;
	//private bool vidurnaktis;
	//private bool[] arSiandienaVaziuoja;
	private int lastUpdate;
	private string[,] stotele;


	public Vector2[] trumpasSarasas;

	//transportas laikas, tipas, id
	static public Vector3[] trans = new Vector3[0];  //0-laikas, 1-tipas(1-autobusas, 2-troleibusas, 3-greitasis, 4-naktinis), 2-id
	static public string[] transName = new string[0];

	/*[System.Serializable]
	public class transportas{
		public int time;
		public string marstutas;
		public string tipas;
	}*/

	/*void TransData() {
		int savaitesDiena = LaikasDabar.savaitesDiena;
		int laikasMinutem = LaikasDabar.laikasMinutem;
		//laikasMinutem = 1430;

		List<int> sarasas = new List<int> ();
		for (int i = 1; i < stotele.GetLength (1); i++) {
			int atvaziuosPo = int.Parse (stotele [0, i]) - laikasMinutem;
			if (atvaziuosPo > -1  && int.Parse (stotele [4+savaitesDiena, i]) == 1) {
				sarasas.Add (i);
			}
		}
		trans = new transportas[sarasas.Count];	
		for (int y = 0; y < sarasas.Count; y++) {
			trans [y].time = 1;	
			//trans [y].time = 1;//int.Parse (stotele [0, sarasas[y]]);
			//trans [y].marstutas = stotele [4, sarasas[y]];
			//trans [y].tipas = stotele [3, sarasas[y]];

		}
	}*/
	void Transportas(){
		trans = new Vector3[trumpasSarasas.Length];
		transName = new string[trumpasSarasas.Length];

		for (int i = 0; i <trans.Length; i++) {
			float laikas = trumpasSarasas [i].x;
			
			int data = (int)trumpasSarasas [i].y; // KURIS DATA
			transName[i] = transportoNumeris [data];
			float tipas = (float)transportoTipas [data];
			float id = (float)transportasServiceId [data];
			
			trans [i] = new Vector3 (laikas, tipas, id);
			
		}
	}


	void TrumpasasSarasas() {
		int list = 6;
		if (transportasLaikas.Length < 6)list = transportasLaikas.Length;
		trumpasSarasas = new Vector2[list];

		for (int i = 0; i <trumpasSarasas.Length; i++) {
			trumpasSarasas[i] =  new Vector2(-1,-1);
			for (int y = 0; y < transportasLaikas.Length; y++) {
				float laikas = transportasLaikas [y];
				bool jauSarase = false;
				for (int z = 0; z <trumpasSarasas.Length; z++) {
					if (trumpasSarasas [z].y == y) jauSarase = true;
				}
				if(!jauSarase) {
					if (laikas < trumpasSarasas [i].x || trumpasSarasas [i].x == -1) {
						trumpasSarasas[i] = new Vector2(laikas, y);
					}
				}
			}
		}	
	}

	void KasVaziuoja() {
		int savaitesDiena = LaikasDabar.savaitesDiena;
		int laikasMinutem = LaikasDabar.laikasMinutem;
		//laikasMinutem = 1430;
			
		List<int> sarasas = new List<int> ();
		for (int i = 1; i < stotele.GetLength (1); i++) {
			int atvaziuosPo = int.Parse (stotele [0, i]) - laikasMinutem;
			if (atvaziuosPo > -1 && atvaziuosPo < 60 && int.Parse (stotele [4+savaitesDiena, i]) == 1) {
				sarasas.Add (i);
			}
		}

		transportasLaikas = new int[sarasas.Count];
		transportoNumeris = new string[sarasas.Count];
		transportoTipas = new int[sarasas.Count];
		transportasServiceId = new int[sarasas.Count];
		//koks tipas i skaicius
		for (int y = 0; y < sarasas.Count; y++) {
			transportasLaikas [y] = int.Parse (stotele [0, sarasas[y]]) - laikasMinutem;
			transportoNumeris  [y] = stotele [4, sarasas[y]];
			string id = stotele [3, sarasas[y]];
			switch (id)
			{
			case "bus":
				transportoTipas [y] = 1;
				break;
			case "trol":
				transportoTipas [y] = 2;
				break;
			case "expressbus":
				transportoTipas [y] = 3;
				break;
			case "nughtbus":
				transportoTipas [y] = 4;
				break;
			default :
				transportoTipas [y] = 0;
				break;
			}
		transportasServiceId[y] = int.Parse (stotele [2, sarasas[y]]);
		}
	}

	void Update() {
		//stoteleInf = stotelesNr;	

		if (LaikasDabar.laikasMinutem == lastUpdate && stotelesNr == stotelesNrOld)return;

		lastUpdate = LaikasDabar.laikasMinutem;
		if(stotelesNr == 0)return;

		loadStotele (stotelesNr);

		KasVaziuoja ();
		TrumpasasSarasas ();
		Transportas ();

	}

	void loadStotele(int nr) {
		if (stotelesNr == stotelesNrOld)return;
		stotelesNrOld = stotelesNr;
		stoteleData = Resources.Load("stoteles/"+nr) as TextAsset;
		stotele = CSVReader.SplitCsvGrid(stoteleData.text);
	}


}

