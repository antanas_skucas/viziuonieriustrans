﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class StotelesSimple : MonoBehaviour {

	public TextAsset stops;
	public Text infoText;
	public string artimiausiaStotele;

	static public string[,] stoteles; //1 - eilute pagal pavadinima 2-turinys;
	static public int ilgis;//kiek stoteliu
	static public int plotis;//kiek yra informacijos
	static public Vector2[] stotelesXY; //stoteles padetis metrais
	public int stotele; // artimiausios stoteles id
	public int stoteleId;
	//public int stoteleOld;// priestai nustatytos stoteles id
	//static public Vector2[] stotelesXYTest;
	private Vector2[] stotelesLonLan;//stoteles padetis lon
	private int[] id;//stoteles id
	private string[] stotelesPavadinimas;

	void Update() {
		if (LocationService.position == LocationService.lastPosition)return;
		stotele = ArtimiausiaStotelė (LocationService.position);
		artimiausiaStotele = stotelesPavadinimas [stotele];
		stoteleId = id [stotele];
		UpdateStoteleInfo ();

		if (infoText) {
			//infoText.text = artimiausiaStotele;

			if (StotelesInformacija.stotelesNr > 0) {
				infoText.text = "Artimiausia stotelė: " + StotelesInformacija.pavadinimas + " / " + StotelesInformacija.stotelesNr;	
			} else {
				infoText.text = "Prieikite arčiau stotelės ženklo";
			}
			if (!LocationService.gpsOn)infoText.text = "kad veiktų reikalingas GPS signalas";
		}

	}

	void UpdateStoteleInfo() {
		StotelesInformacija.pavadinimas = artimiausiaStotele;
		StotelesInformacija.stotelesNr = stoteleId;
	}

	int ArtimiausiaStotelė (Vector2 vieta) {
		float atstumas = 100f;
		int artimStotele = 0;

		for (int i = 1; i < ilgis; i++) {
			float a = Vector2.Distance(vieta, stotelesXY[i]);
			if (a < atstumas) {
				atstumas = a;
				artimStotele = i;
			}
		}
		return artimStotele;
	}

	void Start () {
		stoteles = CSVReader.SplitStops(stops.text, 6);
		plotis = stoteles.GetLength (0);
		ilgis = stoteles.GetLength (1);
	
		stotelesPavadinimas = new string[ilgis];
		stotelesLonLan = new Vector2[ilgis];
		stotelesXY = new Vector2[ilgis];
		id = new int[ilgis];

		//stotelių pavadinimai lokacijos id
		for (int i = 1; i < ilgis; i++) {
			stotelesLonLan[i] = new Vector2(float.Parse(stoteles[4,i]), float.Parse(stoteles[5,i]));
			stotelesXY [i] = GpsPlacer.LatLonToMetersNew (stotelesLonLan [i].x, stotelesLonLan [i].y);
			stotelesPavadinimas [i] = stoteles [2, i];
			id[i] = int.Parse (stoteles [0, i]);//stoteles id naudojamas atpažinimui
		}
	}



}
