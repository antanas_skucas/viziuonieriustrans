﻿using UnityEngine;
using System.Collections;
//using System.Collections.Generic;
using System;

public class LaikasDabar : MonoBehaviour {

	private DateTime dateValue;//dabartinis laikas

	// savaites diena ir iseigines

	static public int savaitesDiena;
	static public int laikasMinutem;

	//public TextAsset Sventes;
	//public bool svente;//jeigu šventinė diena;
	private int lastUpdateHour = -1;//kad savaites diena atnaujintu kas valandą

	//laikas
	static public string laikas;


	void Start() {

	}
	
	void Update() {
		dateValue = DateTime.Now;
		Laikas ();
		SaveitesDiena ();
	}

	// valandos ir minutes bei sekundes
	void Laikas() {
		laikasMinutem = dateValue.Hour * 60 + dateValue.Minute;
		laikas = dateValue.ToString("HH:mm:ss");
	}

	//savaite
	void SaveitesDiena() {
		if (dateValue.Hour == lastUpdateHour)return;
		lastUpdateHour = dateValue.Hour;
		savaitesDiena  = (int)dateValue.DayOfWeek;
	}

}
