﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;



public class LocationService : MonoBehaviour {
	//nesunaikina location serviso
	public static LocationService myInstance;

	// testine vieta editoriui kai neveikia gps
	public bool fake = true;

	// location service tikslumas
	public float gpsTikslumas = 0.3f;
	public GameObject[] showAfterLoad;
	public GameObject[] showBeforeLoad;
	public Vector2 vieta;//debug nenaudojama

	public Vector2 fakeGps;
	public Text[] debugText;
	public Vector2 atstumas;

	//static vieta ir pozicija vektoriais
	static public double laikas;
	static public double laikasLast;
	static public double startLaikas;


	static public Vector2 location;
	static public Vector2 position;
	static public Vector2 lastPosition;

	//static public Vector2 kalmanLocation;
	//static public Vector2 kalmanPosition;


	static public Vector2 stablePosition;
	static public Vector2 stableLocation;


	static public Vector2 startLocation;
	static public Vector2 startPosition;
	static public float tikslumasOnStartInst;

	static public int gpsState = 0;
	static public bool gpsOn;
	static public float tikslumas;
	static public double lat;
	static public double lon;



	static public Vector2 tileSizeInDeg;
	static public float earthRadus = 6371;

	//private
	private const int EarthRadius = 6378137;
	private const double OriginShift = 2 * Mathf.PI * EarthRadius / 2;


	static public float greitis = 0;
	private int id = 0;
	static public int gpsUpdatedTimes = 0;
	private Vector3[] vietos= new Vector3[10];

	void Speed() {
		if (startLocation == Vector2.zero)return;
		if (vietos[id].z == (laikas - startLaikas))return;
		Vector2 vieta = position - startPosition;

		if (gpsUpdatedTimes > vietos.Length) {
			Vector2 nuejo = Vector2.zero ;
			for (int i = 1; i < vietos.Length;++i) {
				float time = Mathf.Abs(vietos [i - 1].z-vietos [i].z);
				nuejo += new Vector2 ((vietos [i - 1].x - vietos [i].x)/time, (vietos [i - 1].y - vietos [i].y)/time);
			}
			greitis = Vector2.Distance(Vector2.zero, nuejo)/vietos.Length;
		}

		++id;
		id = id % vietos.Length;
		vietos [id] = new Vector3 (vieta.x,vieta.y,(float)(laikas-startLaikas));
	}

	void ShowHide(bool rodyti) {
		foreach (GameObject obj in showAfterLoad)
			obj.SetActive (rodyti);
		foreach (GameObject obj in showBeforeLoad)
			obj.SetActive (!rodyti);
	}

	void Awake() {

		if (!Application.isEditor)fake = false;
		if (fake) {
			lat = fakeGps.x;
			lon = fakeGps.y;
			gpsState = 1;
			ShowHide (true);
		} else {
			ShowHide(false);
		}

		if (myInstance == null) {
			myInstance = this; 
		} else {
			Destroy (gameObject);
		}
	
		DontDestroyOnLoad (transform.gameObject);
	
	}

	void Start() {
		StartCoroutine(StartGps());
	}

	void FakeMove(){
		float speed = 0.005f;
		fakeGps.x += Input.GetAxis ("Vertical") * Time.deltaTime * speed;
		fakeGps.y += Input.GetAxis ("Horizontal")*Time.deltaTime * speed;
	}
		
	IEnumerator StartGps ()
	{
		// First, check if user has location service enabled
		if (!Input.location.isEnabledByUser)
			yield break;

		// Start service before querying location
		Input.location.Start(gpsTikslumas,gpsTikslumas);

		// Wait until service initializes
		int maxWait = 20;
		while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
		{
			yield return new WaitForSeconds(1);
			maxWait--;
		}

		// Service didn't initialize in 20 seconds
		if (maxWait < 1)
		{
			print("Timed out");
			yield break;
		}

		// Connection has failed
		if (Input.location.status == LocationServiceStatus.Failed)
		{
			print("Unable to determine device location");
			yield break;
		}
		else
		{
			print ("cha cha cha turetu veikti");
			gpsOn = true;
			gpsState = 1;
			ShowHide(true);
			updateGpsData ();
			//ideda start pozicija


		}
	}

	static public Vector2 Atstumas(double lat, double lon) {
		return new Vector2 (2 * Mathf.PI * earthRadus*(float)lat / 360, 2 * Mathf.PI* earthRadus*(float)lon / 360);
	}

	static public int LaipsnisToTile(float laipsnis, float mastelis) {
		int result = (int)Mathf.Round (360 / laipsnis);
		return result;
	}


	// Access granted and location value could be retrieved
	void updateGpsData() {
		if (!gpsOn) {
			if (!fake)return;
			FakeMove ();
			lat = fakeGps.x;
			lon = fakeGps.y;
			tikslumas = 3;
			laikas = Time.time;
		} else {
			laikas = Input.location.lastData.timestamp;
			lat = Input.location.lastData.latitude;
			lon = Input.location.lastData.longitude;
			tikslumas = Input.location.lastData.horizontalAccuracy;//(Input.location.lastData.horizontalAccuracy+Input.location.lastData.verticalAccuracy)/2;
		}

		if (laikas != laikasLast) {
			++gpsUpdatedTimes;
			laikasLast = laikas;
			stabilizatePosition ();
		}

		if (Vector2.zero == startLocation) {
			LocationToStart ();
		}
		location = new Vector2 ((float)lat, (float)lon);
		position = LocationToMeters();
		vieta = location;//tik debug nenaudojama
		atstumas = Atstumas (lat,lon);
		Speed();

	}

	void stabilizatePosition() {
		if (Vector2.Distance (lastPosition, position) < Mathf.Clamp((tikslumas/2), 0f, 3f))return;
		stablePosition = position;
		stableLocation = location;
		lastPosition = position;
	}

	void Update() {
		if (gpsState == 0)return;
		updateGpsData ();
		debugTxt ();
	}

	void debugTxt(){
		if (debugText.Length == 0)return;
		if (debugText [0])debugText [0].text = "acc:" + Input.acceleration;
	}

	public static void UpdateStartLocation(Vector2 vieta) {
		startLocation = vieta;
		startPosition = LatLonToMeters (vieta);
		tikslumasOnStartInst = tikslumas;
		startLaikas = laikas;
	}

	public static void LocationToStart() {
		startLocation = location;
		startPosition = LatLonToMeters (location);
		tikslumasOnStartInst = tikslumas;
		startLaikas = laikas;
	}

	public static Vector2 LatLonToMeters(Vector2 vieta)
	{
		var p = new Vector2();
		p.x = (float)(vieta.y * OriginShift / 180);
		p.y = (float)(Mathf.Log(Mathf.Tan((90 + vieta.x) * Mathf.PI / 360)) / (Mathf.PI / 180));
		p.y = (float)(p.y * OriginShift / 180);
		return new Vector2(p.y, p.x);
	}
	public static Vector2 LocationToMeters()
	{
		var p = new Vector2();
		p.x = (float)(location.y * OriginShift / 180);
		p.y = (float)(Mathf.Log(Mathf.Tan((90 + location.x) * Mathf.PI / 360)) / (Mathf.PI / 180));
		p.y = (float)(p.y * OriginShift / 180);
		return new Vector2(p.y, p.x);
	}

	public static Vector3 metraiTo3d(Vector2 vieta, float mastelis) {
		Vector2 pozicija = vieta - startPosition;
		return new Vector3(-pozicija.y*mastelis,0,-pozicija.x*mastelis);
	}


}
