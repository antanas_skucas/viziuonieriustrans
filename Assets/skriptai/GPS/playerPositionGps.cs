﻿using UnityEngine;
using System.Collections;

public class playerPositionGps : MonoBehaviour {

	public bool asZaidimocentras;
	public float mastelis = 1f;
	public float ugis = 1.5f;
	public bool testas;
	public bool updateOnMove;
	void Update () {
		if (asZaidimocentras) {
			LocationService.LocationToStart ();
			if (LocationService.tikslumas < 10)	asZaidimocentras = false;
		}
		/*if (updateOnMove) {
			if (!GyroMove.moving)return;
		}*/
		Vector3 padetis = Vector3.zero;
		if (testas) {
			//padetis = GpsPlacer.PlaceObjSuMasteliu (LocationService.kalmanLocation.x, (float)LocationService.kalmanLocation.y, mastelis);
			padetis = LocationService.stablePosition-LocationService.startPosition;
		} else {
			padetis = GpsPlacer.PlaceObjSuMasteliu ((float)LocationService.lat, (float)LocationService.lon, mastelis);
		}
		transform.position = new Vector3(padetis.x, ugis, padetis.z);
	}

	public void starpPosition() {
		LocationService.LocationToStart ();
	}
}
