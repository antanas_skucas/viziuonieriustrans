﻿using UnityEngine;
using System.Collections;

public class GpsPlacer : MonoBehaviour {

	public bool noMap;
	public GameObject Player;
	public GameObject Glider;
	public float zemelapioMastelis = 1000;

	//static
	static public float mastelis = 1f;

	//private
	private const int EarthRadius = 6378137;
	public const double OriginShift = 2 * Mathf.PI * EarthRadius / 2;


	void Awake() {
		mastelis = 1/zemelapioMastelis;
	}


	void Start() {
		//UpdatePos ();
	}
		
	public static Vector3 PlaceObj(float lat, float lon) {
		float latStart = LocationService.startLocation.x;
		float lonStart = LocationService.startLocation.y;
		//Vector2 centras = LatLonToMeters (OpenStreatStamen.worldStarLatLon.x, OpenStreatStamen.worldStarLatLon.y);

		Vector2 centras = LatLonToMetersNew (latStart, lonStart);
		Vector2 objektas = LatLonToMetersNew (lat, lon);
		Vector2 pozicija = objektas - centras;
		return new Vector3(-pozicija.y*mastelis,0,-pozicija.x*mastelis);
	}

	public static Vector3 PlaceObjSuMasteliu(float lat, float lon, float mastelis) {
		float latStart = LocationService.startLocation.x;
		float lonStart = LocationService.startLocation.y;

		Vector2 centras = LatLonToMetersNew (latStart, lonStart);
		Vector2 objektas = LatLonToMetersNew (lat, lon);
		Vector2 pozicija = objektas - centras;
		return new Vector3(-pozicija.y*mastelis,0,-pozicija.x*mastelis);
	}



	public static Vector3 Locator(Vector2 centrasLatLon, Vector2 latLon) {
		Vector2 centras = LatLonToMeters (centrasLatLon.x, centrasLatLon.y);
		Vector2 objektas = LatLonToMeters (latLon.x, latLon.y);
		Vector2 pozicija = objektas - centras;
		return new Vector3(-pozicija.y,0,-pozicija.x);
	}

	public static Vector2 LatLonToMeters(float lat, float lon)
	{
		var p = new Vector2();
		p.x = (float)(lon * OriginShift / 180);
		p.y = (float)(Mathf.Log(Mathf.Tan((90 + lat) * Mathf.PI / 360)) / (Mathf.PI / 180));
		p.y = (float)(p.y * OriginShift / 180);
		return new Vector2(p.y*mastelis, p.x*mastelis);
	}

	//be mastelio
	public static Vector2 LatLonToMetersNew(float lat, float lon)
	{
		var p = new Vector2();
		p.x = (float)(lon * OriginShift / 180);
		p.y = (float)(Mathf.Log(Mathf.Tan((90 + lat) * Mathf.PI / 360)) / (Mathf.PI / 180));
		p.y = (float)(p.y * OriginShift / 180);
		return new Vector2(p.y, p.x);
	}

	/*void UpdatePos () {
		float latStart = LocationService.startLocation.x;
		float lonStart = LocationService.startLocation.y;
		//float latStart = OpenStreatStamen.worldStarLatLon.x;
		//float lonStart = OpenStreatStamen.worldStarLatLon.y;

		for (int i = 0; i < gpsObjektai.Length; i++) {
			Vector2 pozicija = LatLonToMeters (gpsObjektai [i].latitude,  gpsObjektai [i].longitude)-LatLonToMeters (latStart,lonStart) ;
			gpsObjektai[i].objektas.transform.localPosition = new Vector3(-pozicija.y,0,-pozicija.x);
		}
	}*/


}
