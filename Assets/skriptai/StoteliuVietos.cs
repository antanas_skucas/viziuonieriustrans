﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class StoteliuVietos : MonoBehaviour {

	//static
	static public string[,] stoteles; //1 - eilute pagal pavadinima 2-turinys;
	static public int ilgis;
	static public int plotis;
	static public Vector2[] stotelesXY;
	static public Vector2[] stotelesXYTest;
	static public Vector2[] stotelesLonLan;

	// public
	public Text infoText;
	public TextAsset csv; 
	//public Vector2 myLocation;
	public string artimiausiaStotele;

	static public int[] id;
	static public string[] stotelesPavadinimas;

	void Update() {
		if (LocationService.position == LocationService.lastPosition)return;
		//myLocation = LocationService.position;

		artimiausiaStotele = stotelesPavadinimas [ArtimiausiaStotelė (LocationService.position)];
		if (infoText)infoText.text = artimiausiaStotele;

	}

	int ArtimiausiaStotelė (Vector2 vieta) {
		float atstumas = 100f;
		int artimStotele = 0;

		for (int i = 1; i < ilgis; i++) {
			stotelesXYTest[i] = stotelesXY[i]-vieta;
			float a = Vector2.Distance(vieta, stotelesXY[i]);
			if (a < atstumas) {
				atstumas = a;
				artimStotele = i;
			}
		}
		//if(atstumas>40f) artimStotele = 0;
		return artimStotele;
	}

	void Start () {

		stoteles = CSVReader.SplitCsvGrid(csv.text);
		plotis = stoteles.GetLength (0);
		ilgis = stoteles.GetLength (1);

		Debug.Log (ilgis);

		stotelesPavadinimas = new string[ilgis];
		stotelesLonLan = new Vector2[ilgis];
		stotelesXY = new Vector2[ilgis];
		stotelesXYTest  = new Vector2[ilgis];
		id = new int[ilgis];
		for (int i = 1; i < ilgis; i++) {
			stotelesLonLan[i] = new Vector2(float.Parse(stoteles[1,i]), float.Parse(stoteles[0,i]));
			stotelesXY [i] = GpsPlacer.LatLonToMetersNew (stotelesLonLan [i].x, stotelesLonLan [i].y);
			stotelesPavadinimas [i] = stoteles [3, i];
			id[i] = int.Parse (stoteles [2, i]);
		}
	}

}
